package br.unb.biodyn.test;

import br.unb.biodyn.model.Counter;
import junit.framework.TestCase;

public class CounterTest extends TestCase {

	private Counter counter;
	
	public void testStartWithParameterValue() throws Exception {
		int value = 10;
		this.counter = new Counter(value);
		
		assertEquals(value, this.counter.getValue());		
	}

	public void testStartInitialValue() throws Exception {
		this.counter = new Counter();
		assertEquals(Counter.INITIAL_VALUE, this.counter.getValue());		
	}
	
	public void testIncreaseCounterValue() throws Exception {
		int value;
		this.counter = new Counter();
		value = this.counter.getValue();
		this.counter.increase();
		assertEquals(value+1, this.counter.getValue());		
	}
	
	public void testDecreaseCounterValue() throws Exception {
		int value;
		this.counter = new Counter();
		value = this.counter.getValue();
		this.counter.decrease();
		assertEquals(value-1, this.counter.getValue());
	}
	
}


