package br.unb.biodyn.model;

public class Exercise {
	
	private static final double PERCENT = 0.10;
	private float maximumWeight;
	private float minimumWeight;
	
	public Exercise(float maximumWeight, float minimumWeight) {
		this.maximumWeight = maximumWeight;
		this.minimumWeight = minimumWeight;
	}

	public float getMaximumWeight() {
		return maximumWeight;
	}

	public void setMaximumWeight(float maximumWeight) {
		this.maximumWeight = maximumWeight;
	}

	public float getMinimumWeight() {
		return minimumWeight;
	}

	public void setMinimumWeight(float minimumWeight) {
		this.minimumWeight = minimumWeight;
	}
	
	public void countLimitValuesPercentual(float maximumWeight, float minimumWeight) {
		float limitValueMaximum;
		float limitValueMinimum;
		float limitValuePercent;
		
		limitValuePercent = (float) ((maximumWeight - minimumWeight) * PERCENT);

		limitValueMaximum = maximumWeight - limitValuePercent;
		limitValueMinimum = minimumWeight + limitValuePercent;
	}
	
	public void countSeriesValue () {
		
	}
	
	public void countRestTime () {
		
	}
}
