package br.unb.biodyn.model;
import java.util.Timer;
import java.util.TimerTask;

import android.os.CountDownTimer;
import android.util.Log;
import android.widget.TextView;


public class Counter {
	
	private Timer timer;
	private TimerTask timerTask; 
	private TextView text;
	
	public static final int INITIAL_VALUE = 0;

	private int value;
	
	public Counter(int value) {
		this.value = value;
	}
	
	public Counter() {
		this(INITIAL_VALUE);
//		this.start = new StartTimer(10000, 1000);
//		start.start();
	}
	
	public void increase() {
		this.value++;
	}
	
	public void decrease() {
		this.value--;
	}
	
	public int getValue() {
		return this.value;
	}
	
//	public void start(){
//		new StartTimer(10000, 1000).start();
//	}
	
}
