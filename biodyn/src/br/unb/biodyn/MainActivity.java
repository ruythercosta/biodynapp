package br.unb.biodyn;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	/********************************** CONSTANTS ***********************************/

	// Debugging
	private static final String TAG = "BluetoothChat";
	private static final boolean DEBUG = true;

	// Intent request code for result checking
	private static final int REQUEST_CONNECT_DEVICE = 1;
	private static final int REQUEST_ENABLE_BT = 2;

	/********************************** CLASS FIELDS ***********************************/

	private String mConnectedDeviceName = null;
	private BluetoothAdapter mBluetoothAdapter = null;
	private mReceiver myReceiver;

	/********************************** LIFECYCLE METHODS ***********************************/

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (DEBUG)
			Log.d(TAG, "+++ ON CREATE +++");
		setContentView(R.layout.main);

		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		if (mBluetoothAdapter == null) {
			Toast.makeText(this, "Bluetooth is not available on this device!", Toast.LENGTH_LONG).show();
			this.finish();
			return;
		}
		
		myReceiver = new mReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction(BluetoothService.ACTION_DATA_RECEIVED);
		filter.addAction(BluetoothService.ACTION_MESSAGE_DEVICE_NAME);
		filter.addAction(BluetoothService.ACTION_MESSAGE_SHOW_TOAST);
		LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver, filter);
		
	}

	@Override
	public synchronized void onResume() {
		if (DEBUG)
			Log.d(TAG, "+ ON RESUME +");
		super.onResume();
	}

	@Override
	public void onDestroy() {
//		Intent i = new Intent(BluetoothService.ACTION_BLUETOOTH_SERVICE);
//		stopService(i);
		LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver);
		if (DEBUG)
			Log.d(TAG, "--- ON DESTROY ---");
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.option_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.scan:
			enableBlueTooth();
			// Launch the DeviceListActivity to see devices and do scan
			Intent serverIntent = new Intent(this, DeviceListActivity.class);
			startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
			return true;
		case R.id.discoverable:
			ensureDiscoverable();
			return true;
		}
		return false;
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (DEBUG)
			Log.d(TAG, "onActivityResult " + resultCode);
		switch (requestCode) {
		case REQUEST_CONNECT_DEVICE:
			if (resultCode == Activity.RESULT_OK) {
				String address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
				BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
				try {
					Intent intent = new Intent(BluetoothService.ACTION_CONNECT_TO_DEVICE);
					intent.putExtra(BluetoothService.KEY_DEVICE_TO_CONNECT, device);
					LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
				} catch (Exception e) {
					Log.e(TAG, "onActivityResult got an error!", e);
				}
			}
			break;
		case REQUEST_ENABLE_BT:
			if (resultCode == Activity.RESULT_OK) {
				Intent intent = new Intent(BluetoothService.ACTION_BLUETOOTH_SERVICE);
				startService(intent);
			} else {
				// User did not enable Bluetooth or an error occured
				Log.d(TAG, "BlueTooth not enabled");
				Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
			}
		}
	}

	/********************************** OTHER METHODS ***********************************/

	//TODO remove this method and its menu button when Bluetooth communication is done
	private void ensureDiscoverable() {
		if (DEBUG)
			Log.d(TAG, "ensure discoverable");
		if (mBluetoothAdapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
			Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
			discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
			startActivity(discoverableIntent);
		}
	}

	private void enableBlueTooth() {
		if (!mBluetoothAdapter.isEnabled()) { //Enable bluetooth and start bluetooth service
			Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
		} else { //Bluetooth already enabled, just start the service
			Intent intent = new Intent(BluetoothService.ACTION_BLUETOOTH_SERVICE);
			startService(intent);
		}
	}

	
	/********************************** PRIVATE CLASSES ***********************************/
	
	private class mReceiver extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if(action.equals(BluetoothService.ACTION_DATA_RECEIVED)){
				//byte[] readBuf = intent.getExtras().getByteArray(BluetoothService.KEY_DATA_RECEIVED);
				//String readMessage = new String(readBuf, 0, intent.getExtras().getInt(BluetoothService.KEY_DATA_RECEIVED_SIZE));

				//TODO handle message
				TextView text = (TextView) findViewById(R.id.text);
				text.setText(intent.getExtras().getString(BluetoothService.KEY_DATA_RECEIVED));
				
			}else if (action.equals(BluetoothService.ACTION_MESSAGE_DEVICE_NAME)){
				 //save the connected device's name
				mConnectedDeviceName = intent.getExtras().getString(BluetoothService.KEY_DEVICE_NAME);
				Toast.makeText(getApplicationContext(), "Connected to " + mConnectedDeviceName, Toast.LENGTH_SHORT)
						.show();
			} else if(action.equals(BluetoothService.ACTION_MESSAGE_SHOW_TOAST)){
				Toast.makeText(getApplicationContext(), intent.getExtras().getString(BluetoothService.KEY_TOAST_MESSAGE),
				Toast.LENGTH_SHORT).show();
			}
		}
		
	}
}