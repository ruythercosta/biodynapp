package br.unb.biodyn.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import br.unb.biodyn.R;
import br.unb.biodyn.model.Counter;

public class CounterUI extends RelativeLayout implements OnClickListener {

	private TextView textCounter;
	private Button buttonIncrease;
	private Button buttonDecrease;
	private Counter counter;
	
	public CounterUI(Context context, AttributeSet attrs) {
		super(context, attrs);
		inflate(context, R.layout.counter_components, this);
		this.counter = new Counter();
		initializeComponents();
		updateTextCounter();
	}

	@Override
	public void onClick(View view) {
		if(view.getId() == this.buttonIncrease.getId()) {
			this.counter.increase();
			updateTextCounter();
			System.out.println("right");
		} else if(view.getId() == this.buttonDecrease.getId()) {
			this.counter.decrease();
			updateTextCounter();
		}
	}
	
	private void updateTextCounter() {
		this.textCounter.setText(String.valueOf(this.counter.getValue()));
	}
	
	private void initializeComponents() {
		this.textCounter = (TextView)findViewById(R.id.textCounter);
		this.buttonDecrease = (Button)findViewById(R.id.buttonDecrease);
		this.buttonDecrease.setOnClickListener(this);
		this.buttonIncrease = (Button)findViewById(R.id.buttonIncrease);
		this.buttonIncrease.setOnClickListener(this);
	}
	
	
}
