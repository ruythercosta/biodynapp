package br.unb.biodyn.ui;

import android.content.Context;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import br.unb.biodyn.R;

public class TimerUI extends RelativeLayout implements OnClickListener {

	private TextView textTimerCounter;
	private Button buttonIncrease;
	private Button buttonDecrease;
	//Duration of rest time
	private long duration = 10000;
	private static final long ONTICK_DELAY = 2000;
	
	//Time of update interval on CountDownTimer
	private long onInterval = 1000;
		
	public TimerUI(Context context, AttributeSet attrs) {
		super(context, attrs);
		inflate(context, R.layout.counter_components, this);
		initializeComponents();
	}
	
	@Override
	public void onClick(View view) {
		if(view.getId() == this.buttonIncrease.getId()) {
			new StartTimer((duration + ONTICK_DELAY), onInterval).start();	
		}
	}
	
	//Method to start CountDownTimer
	public void StartTimer(){
			
	}
	
	private void initializeComponents() {
		this.textTimerCounter = (TextView)findViewById(R.id.textCounter);
		this.buttonIncrease = (Button)findViewById(R.id.buttonIncrease);
		this.buttonIncrease.setOnClickListener(this);
		this.buttonDecrease = (Button)findViewById(R.id.buttonDecrease);
		this.buttonDecrease.setOnClickListener(this);
	}
		
public class StartTimer extends CountDownTimer{
				
		private static final int MILLISECONDS_TO_MINUTES = 60000;
		private static final int MILLISECONDS_TO_SECONDS = 1000;
		private static final int FORMAT_SECONDS = 10;
		private Long minutes;
		private Long seconds;
		private String totalTime;	
		
			
		public StartTimer(long duration, long onInterval) {
			super(duration, onInterval);
		}		
		
		@Override
		public void onTick(long millisUntilFinished) {
			this.minutes = duration/MILLISECONDS_TO_MINUTES;
			this.seconds = (duration%MILLISECONDS_TO_MINUTES)/MILLISECONDS_TO_SECONDS;
			if(seconds < FORMAT_SECONDS) {
				totalTime = this.minutes.toString()+":0"+this.seconds.toString();
			}
			else {
				totalTime = this.minutes.toString()+":"+this.seconds.toString();				
			}			
			textTimerCounter.setText(String.valueOf((totalTime)));
			duration = (duration - 1000);
		}

		@Override
		public void onFinish() {
			textTimerCounter.setText("0:00");
		}
	}
}
